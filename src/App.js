import React from 'react';

import './App.css';
import Pagina from './component/Pagina';

function App() {
  return (
    <div>
      <Pagina/>
    </div>
  );
}

export default App;
