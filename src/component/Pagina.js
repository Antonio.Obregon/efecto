
import React, {Component} from 'react';
import './style.css';

class Pagina extends Component{

    constructor(props){
        super(props);
        this.efecto = this.efecto.bind(this);
        this.state = {
            clase: false,
        };
    }
    

efecto = (clase)=>{
    
    this.setState({clase:!this.state.clase});
    console.log(this.state.clase);
}

    render() {
        return(
            <div>
                <nav className={this.state.clase ? "active":""}>
                    <ul>
                        <li>HOME</li>
                        <li>GALLERY</li>
                        <li>SERVICE</li>
                        <li>TEAM</li>
                        <li>LOCATION</li>
                    </ul>

                </nav>
                <div className="container">
                    <h1>High Drop Navigation</h1>
                    <br></br>

                    <p>Create by <a href="">Pedro el perro</a></p>

                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                         Mollitia quaerat quas tempora voluptatum eos vitae nisi maxime velit dolorem, 
                         ipsam ad.
                         Eum harum libero assumenda enim illo ut corporis optio!
                    </p> 

                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                         Mollitia quaerat quas tempora voluptatum eos vitae nisi maxime velit dolorem, 
                         ipsam ad.
                         Eum harum libero assumenda enim illo ut corporis optio!
                    </p>    

                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                         Mollitia quaerat quas tempora voluptatum eos vitae nisi maxime velit dolorem, 
                         ipsam ad.
                         Eum harum libero assumenda enim illo ut corporis optio!
                    </p>    

                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                         Mollitia quaerat quas tempora voluptatum eos vitae nisi maxime velit dolorem, 
                         ipsam ad.
                         Eum harum libero assumenda enim illo ut corporis optio!
                    </p>    

                </div>

                <div className="hamburger" onClick={this.efecto}>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        );

    }
}

export default Pagina